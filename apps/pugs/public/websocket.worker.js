let user = null;
let websockets = new Map();

const getRoomId = ({ id, username }) => `${id}__${username}`;

const getWebsocket = (roomId) =>
  new Promise((resolve) => {
    if (websockets.has(roomId)) {
      resolve(websockets.get(roomId));
    } else {
      const wss =
        'wss://s8178.nyc1.piesocket.com/v3/{{ roomId }}?api_key=rJf6MHzR7kiWgTV4xD5bOwis0llNaa3kjpahvGqj&notify_self=1'.replace(
          /{{([\s\S]+?)roomId([\s\S]+?)}}/g,
          roomId
        );

      const websocket = new WebSocket(wss);

      websocket.onopen = () => resolve(websocket);
    }
  });

const getAccountStore = async (mode) => {
  const transaction = await new Promise((resolve) => {
    const openReq = indexedDB.open('account');

    openReq.onsuccess = () =>
      resolve(openReq.result.transaction('account', mode));
  });

  return transaction.objectStore('account');
};

const toIDBReqPromise = (request) =>
  new Promise((resolve, reject) => {
    request.onerror = (e) => reject(e);

    request.onsuccess = ({ target }) => {
      const { result } = target;

      resolve(result == null ? null : result);
    };
  });

const makeResponse = async ({ request, action, params, sender }) => {
  const websocket = await getWebsocket(getRoomId(sender));

  switch (action) {
    case 'GetProfile': {
      const store = await getAccountStore('readonly');
      const index = store.index('username');
      const { id, username, fullname, description, thumbnail, background } =
        await toIDBReqPromise(index.get(user.username));

      websocket.send(
        JSON.stringify({
          response: request,
          data: { id, username, fullname, description, thumbnail, background },
        })
      );

      if (!websockets.has(getRoomId(sender))) {
        websocket.close();
      }

      break;
    }
    default:
  }
};

self.addEventListener('fetch', (e) => {
  const { method, url } = e.request;
  const pathname = url.replace(self.location.origin, '');

  switch (`${method}:${pathname}`) {
    case 'PATCH:/api/websocket/connect': {
      e.preventDefault();

      e.respondWith(
        caches.match(e.request).then(async () => {
          const { mine, targets } = await e.request.json();

          if (JSON.stringify(user) !== JSON.stringify(mine)) {
            const roomId = getRoomId(mine);
            const myWebsocket = await getWebsocket(roomId);

            Array.from(websockets.values()).forEach((websocket) =>
              websocket.close()
            );

            websockets = new Map([['mine', myWebsocket]]);
            user = mine;

            await Promise.all(
              targets.map(async (target) => {
                const roomId = getRoomId(target);

                if (!websockets.has(roomId)) {
                  websockets.set(roomId, await getWebsocket(roomId));
                }

                return;
              })
            );

            myWebsocket.addEventListener(
              'message',
              async ({ data: message }) => {
                const data = JSON.parse(message);

                if (
                  typeof data.request === 'string' &&
                  typeof data.action === 'string'
                ) {
                  await makeResponse(data);
                } else {
                  const clients = await self.clients.matchAll();

                  clients?.forEach((client) => client.postMessage(data));
                }
              }
            );
          }

          return new Response(null, {
            status: 200,
            statusText: 'Connect Done.',
          });
        })
      );

      break;
    }
    case 'POST:/api/websocket/send': {
      e.preventDefault();

      e.respondWith(
        caches.match(e.request).then(async () => {
          const { targets, data } = await e.request.json();

          const $websockets = await Promise.all(
            targets.map((target) => getWebsocket(getRoomId(target)))
          );

          $websockets.forEach((websocket) =>
            websocket.send(JSON.stringify(data))
          );

          return new Response(null, {
            status: 200,
            statusText: 'Send Done.',
          });
        })
      );

      break;
    }
    case 'PUT:/api/websocket/request': {
      e.preventDefault();

      e.respondWith(
        caches
          .match(e.request)
          .then(async () => {
            const { uid, target, action, params } = await e.request.json();

            return {
              request: uid,
              action,
              params,
              sender: user,
              websocket: await getWebsocket(getRoomId(target)),
            };
          })
          .then(
            ({ websocket, ...req }) =>
              new Promise((resolve) => {
                const myWebsocket = websockets.get('mine');

                const timeoutId = setTimeout(() => {
                  resolve(
                    new Response(null, {
                      status: 408,
                      statusText: 'request-timeout',
                    })
                  );

                  clearTimeout(timeoutId);
                }, 6000);

                myWebsocket.addEventListener(
                  'message',
                  function listener({ data: res }) {
                    const { response, data } = JSON.parse(res);

                    if (req.request === response) {
                      myWebsocket.removeEventListener('message', listener);

                      resolve(
                        new Response(JSON.stringify(data), {
                          status: 200,
                          statusText: 'Request Done.',
                        })
                      );
                    }
                  }
                );

                websocket.send(JSON.stringify(req));
              })
          )
      );

      break;
    }
    default:
  }
});
