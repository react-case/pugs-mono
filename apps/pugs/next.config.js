const { withNx } = require('@nrwl/next/plugins/with-nx');
const { merge } = require('webpack-merge');
const { DefinePlugin } = require('webpack');

const fs = require('fs');
const path = require('path');

/** @type {import('@nrwl/next/plugins/with-nx').WithNxOptions} */
const nextConfig = {
  nx: {
    // Set this to true if you would like to to use SVGR
    // See: https://github.com/gregberge/svgr
    svgr: false,
  },
  webpack: (config, { buildId }) =>
    merge(config, {
      plugins: [
        new DefinePlugin({
          '__WEBPACK_DEFINE__.ENV': JSON.stringify(buildId),
          '__WEBPACK_DEFINE__.LANGUAGES': JSON.stringify(
            fs.readdirSync(path.resolve(__dirname, './src/assets/locales'))
          ),
        }),
      ],
      resolve: {
        alias: {
          '~pugs': path.resolve(__dirname, './src'),
        },
      },
    }),
};

module.exports = withNx(nextConfig);
