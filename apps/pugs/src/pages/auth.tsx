import { useRouter } from 'next/router';

import AuthSwipe from '~pugs/features/app/AuthSwipe';

export default function Login() {
  const { replace } = useRouter();

  return <AuthSwipe onLoginSuccess={() => replace('/')} />;
}
