import { Suspense } from 'react';
import { useRouter } from 'next/router';
import { useSnackbar } from 'notistack';
import LinearProgress from '@mui/material/LinearProgress';

import { AccountServiceTypes } from '~pugs/services/account';
import { request } from '~pugs/services/websocket';
import { useAppContext } from '~pugs/contexts/AppContext';
import { useAccountContext } from '~pugs/contexts/AccountContext';
import useLazy from '~pugs/hooks/useLazy';
import UserProfile from '~pugs/features/profile/UserProfile';

type Userinfo = Omit<AccountServiceTypes.Account, 'password' | 'token'>;

export default function Profile() {
  const { query } = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const { getFixedT } = useAppContext();
  const { userinfo: mine } = useAccountContext();
  const { id, username } = query as Pick<Userinfo, 'id' | 'username'>;
  const at = getFixedT('app');

  const LazyUserProfile = useLazy<any>(async () => {
    if (mine?.id !== id || mine?.username !== username) {
      const userinfo = await request<Userinfo>(
        { id, username },
        { action: 'GetProfile' }
      ).catch(({ response }) => {
        enqueueSnackbar(at(`msg-${response.statusText}`), {
          variant: 'error',
        });

        return null;
      });

      return () => <UserProfile userinfo={userinfo} />;
    }

    return () => <UserProfile userinfo={mine} />;
  }, [mine?.id, mine?.username, id, username]);

  return (
    <Suspense fallback={<LinearProgress />}>
      <LazyUserProfile />
    </Suspense>
  );
}
