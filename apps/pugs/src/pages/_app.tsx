import { useMemo } from 'react';
import { AppProps } from 'next/app';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { connect } from '~pugs/services/websocket';
import { useAccountContext } from '~pugs/contexts/AccountContext';
import AppProvider, { useAppContext } from '~pugs/contexts/AppContext';
import AppMain from '~pugs/features/app/AppMain';
import AuthSwipe from '~pugs/features/app/AuthSwipe';
import BackstageMain from '~pugs/features/app/BackstageMain';
import ServiceWorkerWrapper from '~pugs/features/app/ServiceWorkerWrapper';

export default function App({ Component, pageProps }: AppProps) {
  const { asPath, replace } = useRouter();
  const { initial, isAuthenticated } = useAccountContext();
  const { getFixedT } = useAppContext();
  const at = getFixedT('app');

  const Container = useMemo(
    () => (asPath.startsWith('/backstage') ? BackstageMain : AppMain),
    [asPath]
  );

  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />

        <title>{at('ttl-website')}</title>
      </Head>

      <AppProvider>
        <ServiceWorkerWrapper
          onInit={() =>
            initial()
              .then((userinfo) => connect(userinfo))
              .catch((e) => console.log(e))
          }
        >
          <Container>
            {isAuthenticated ? (
              <Component {...pageProps} />
            ) : (
              <AuthSwipe onLoginSuccess={() => replace('/')} />
            )}
          </Container>
        </ServiceWorkerWrapper>
      </AppProvider>
    </>
  );
}
