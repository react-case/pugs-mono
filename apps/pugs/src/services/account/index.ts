import { generate as uuid } from 'shortid';

import * as Types from './types';

const getStore: Types.StorePromise = async (mode) => {
  if (global.window) {
    const transaction: IDBTransaction = await new Promise((resolve) => {
      const openReq = global.window.indexedDB.open('account');

      openReq.onsuccess = () =>
        resolve(openReq.result.transaction('account', mode));

      openReq.onupgradeneeded = ({ target }) => {
        const store = (target as IDBOpenDBRequest).result.createObjectStore(
          'account',
          { keyPath: 'id' }
        );

        store.createIndex('username', 'username', { unique: true });
        store.createIndex('password', 'password', { unique: false });
        store.createIndex('fullname', 'fullname', { unique: false });
        store.createIndex('description', 'description', { unique: false });
        store.createIndex('thumbnail', 'thumbnail', { unique: false });
        store.createIndex('background', 'background', { unique: false });
        store.createIndex('token', 'token', { unique: true });
        store.createIndex('friends', 'friends', { unique: false });

        resolve(store.transaction);
      };
    });

    return transaction.objectStore('account');
  }

  return null;
};

const toPromise = <T>(request: IDBRequest) =>
  new Promise<T>((resolve, reject) => {
    request.onerror = (e) => reject(e);

    request.onsuccess = ({ target }) => {
      const { result } = target as IDBRequest<T>;

      resolve(result == null ? null : result);
    };
  });

export { Types as AccountServiceTypes };

export const signup: Types.Signup = async ({ password, ...account }) => {
  const store = await getStore('readwrite');

  return await new Promise((resolve) => {
    const addReq = store.add({
      id: uuid(),
      token: null,
      password,
      friends: [],
      ...account,
    });

    addReq.onsuccess = () => resolve({ ...account, friends: [] });
  });
};

export const initial: Types.Initial = async () => {
  const store = await getStore('readonly');
  const index = store.index('token');
  const count = await toPromise<number>(index.count()).catch(() => 0);

  if (count) {
    const token = global.document?.cookie.match(
      new RegExp(/(^| )token=([^;]+)/)
    )?.[2];

    const userinfo = await toPromise<Types.Account>(index.get(token));

    if (userinfo) {
      const {
        id,
        username,
        fullname,
        description,
        thumbnail,
        background,
        friends,
      } = userinfo;

      return {
        id,
        username,
        fullname,
        description,
        thumbnail,
        background,
        friends,
      };
    }
  }

  return await Promise.reject('msg-invalid-token');
};

export const login: Types.Login = async ({ username, password }) => {
  const store = await getStore('readwrite');
  const index = store.index('username');
  const userinfo = await toPromise<Types.Account>(index.get(username));
  const token = uuid();

  if (!userinfo) {
    return await Promise.reject('msg-invalid-username');
  }

  if (userinfo.password !== password) {
    return await Promise.reject('msg-invalid-password');
  }

  store.put({ ...userinfo, token });
  global.document.cookie = `token=${token}`;

  return await initial();
};
