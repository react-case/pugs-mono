export type StorePromise = (
  _mode?: IDBTransactionMode
) => Promise<IDBObjectStore | null>;

export type Friend = Pick<Account, 'id' | 'username'> & { group?: string[] };

export interface Account {
  id: string;
  token?: string;
  username: string;
  password: string;
  fullname: string;
  description?: string;
  thumbnail?: string;
  background?: string;
  friends: Friend[];
}

export type Initial = () => Promise<Omit<Account, 'password' | 'token'>>;

export type Login = (
  _user: Pick<Account, 'username' | 'password'>
) => Promise<Omit<Account, 'password'>>;

export type Signup = (
  _account: Omit<Account, 'id' | 'token' | 'friends'>
) => Promise<false | Omit<Account, 'id' | 'password'>>;
