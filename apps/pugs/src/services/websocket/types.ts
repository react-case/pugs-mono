import { AccountServiceTypes } from '~pugs/services/account';

export type DataAction = 'FriendApplication' | 'GetProfile';

export interface SendData<T> {
  action: DataAction;
  content?: T;
}

export interface RequestParams<T> {
  action: 'GetProfile';
  params?: T;
}

export type Connect = (
  userinfo: Pick<AccountServiceTypes.Account, 'id' | 'username' | 'friends'>
) => Promise<void>;

export type Send = <T>(
  targets: AccountServiceTypes.Account['friends'],
  data: SendData<T>
) => Promise<void>;

export type Request = <R, T = any>(
  target: Pick<AccountServiceTypes.Account, 'id' | 'username'>,
  params?: RequestParams<T>
) => Promise<R>;
