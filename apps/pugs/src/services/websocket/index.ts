import axios from 'axios';
import { generate as uuid } from 'shortid';

import * as Types from './types';

export { Types as WebSocketServiceTypes };

export const connect: Types.Connect = ({ friends, ...userinfo }) =>
  axios.patch('/api/websocket/connect', {
    mine: { id: userinfo.id, username: userinfo.username },
    targets: friends.map(({ id, username }) => ({ id, username })),
  });

export const send: Types.Send = (targets, data) =>
  axios.post('/api/websocket/send', {
    data,
    targets: targets.map(({ id, username }) => ({ id, username })),
  });

export const request: Types.Request = (target, { action, params }) =>
  axios
    .put('/api/websocket/request', {
      uid: uuid(),
      target,
      action,
      params,
    })
    .then(({ data }) => data);
