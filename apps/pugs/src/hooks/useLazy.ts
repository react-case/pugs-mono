import { ComponentType, lazy, useMemo } from 'react';

export default function useLazy<P>(
  fetcher: () => Promise<ComponentType<P>>,
  deps: any[] = []
) {
  return useMemo(
    () =>
      lazy(async () => ({
        default: await fetcher(),
      })),
    [...deps]
  );
}
