import { useEffect } from 'react';

export default function useSWMessage<T>() {
  useEffect(() => {
    if (global.navigator && 'serviceWorker' in global.navigator) {
      global.navigator.serviceWorker.onmessage = ({ data }) => {
        console.log('===========', data);
      };
    }
  }, []);
}
