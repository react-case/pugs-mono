import { createTheme } from '@mui/material/styles';
import error from '@mui/material/colors/deepOrange';
import info from '@mui/material/colors/cyan';
import primary from '@mui/material/colors/blueGrey';
import secondary from '@mui/material/colors/lightBlue';
import success from '@mui/material/colors/lime';
import warning from '@mui/material/colors/amber';

export default createTheme({
  palette: {
    mode: 'dark',
    divider: 'rgba(245,241,227,0.12)',
    background: {
      paper: '#474448',
      default: '#2D232E',
    },
    primary: {
      light: '#C2E2EB',
      main: '#A8D5E2',
      dark: '#85C5D6',
      contrastText: '#fff',
    },
    secondary: {
      light: '#FFFD99',
      main: '#FFFD77',
      dark: '#FFFC47',
      contrastText: '#fff',
    },
    info: {
      light: info[300],
      main: info[500],
      dark: info[800],
      contrastText: '#fff',
    },
    success: {
      light: success[300],
      main: success[500],
      dark: success[800],
      contrastText: '#fff',
    },
    warning: {
      light: warning[300],
      main: warning[500],
      dark: warning[800],
      contrastText: '#fff',
    },
    error: {
      light: error[300],
      main: error[500],
      dark: error[800],
      contrastText: '#fff',
    },
    text: {
      primary: 'rgb(245,241,227)',
      secondary: 'rgba(245,241,227,0.7)',
      disabled: 'rgba(245,241,227,0.5)',
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        containedSecondary: {
          color: '#474448',
          fontWeight: 'bolder',
        },
      },
    },
  },
});
