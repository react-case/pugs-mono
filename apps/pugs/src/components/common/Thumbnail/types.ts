import { AvatarProps } from '@mui/material/Avatar';
import SvgIcon from '@mui/material/SvgIcon';

export interface StyleParams {
  background?:
    | 'none'
    | 'primary'
    | 'secondary'
    | 'info'
    | 'success'
    | 'warning'
    | 'error'
    | 'default'
    | 'paper';
  size?: 'small' | 'medium' | 'large';
}

export interface ThumbnailProps extends StyleParams, AvatarProps {
  icon?: typeof SvgIcon;
  className?: string;
  src?: string;
}
