import Avatar from '@mui/material/Avatar';
import PermIdentityOutlinedIcon from '@mui/icons-material/PermIdentityOutlined';

import * as Styles from './styles';
import * as Types from './types';

export default function Thumbnail({
  background = 'default',
  className,
  icon: Icon = PermIdentityOutlinedIcon,
  size = 'medium',
  src,
  ...props
}: Types.ThumbnailProps) {
  const { classes, cx } = Styles.useThumbnailStyles({ background, size });

  return (
    <Avatar
      alt="thumbnail"
      className={cx(classes.avatar, className)}
      src={src}
      {...props}
    >
      {!src && <Icon className={classes.icon} />}
    </Avatar>
  );
}
