import { makeStyles } from 'tss-react/mui';

import * as Types from './types';

export const useThumbnailStyles = makeStyles<Types.StyleParams>({
  name: 'Thumbnail',
})((theme, { background, size }) => {
  const px = theme.spacing(size === 'small' ? 4 : size === 'large' ? 16 : 8);
  const isColor = !/^(default|paper|none)$/.test(background);

  return {
    avatar: {
      width: px,
      height: px,

      background:
        (isColor
          ? theme.palette[background].main
          : theme.palette.background[background]) || 'none',
    },
    icon: {
      color: theme.palette.text.primary,
      fontSize: px,
    },
  };
});
