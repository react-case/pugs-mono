import { makeStyles } from 'tss-react/mui';

export const useImageStyles = makeStyles({ name: 'ResponsiveImage' })(
  (theme) => ({
    root: {
      width: '100%',
      height: 'auto',
    },
  })
);
