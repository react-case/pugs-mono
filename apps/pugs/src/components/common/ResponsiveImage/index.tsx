import Image, { ImageProps } from 'next/image';

import * as Styles from './styles';

export default function ResponsiveImage({ className, ...props }: ImageProps) {
  const { classes, cx } = Styles.useImageStyles();

  return <Image className={cx(classes.root, className)} {...props} />;
}
