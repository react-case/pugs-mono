import CardMedia from '@mui/material/CardMedia';
import { withStyles } from 'tss-react/mui';

export default withStyles(
  CardMedia,
  (theme) => ({
    root: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      overflow: 'hidden',
    },
  }),
  { name: 'MiddleCardMedia' }
);
