import NextLink from 'next/link';
import PetsIcon from '@mui/icons-material/Pets';
import { LinkProps } from '@mui/material/Link';

import { useAppContext } from '~pugs/contexts/AppContext';
import * as Styles from './styles';

export default function LogoLink({ href = '/', ...props }: LinkProps) {
  const { getFixedT } = useAppContext();
  const gt = getFixedT('app');

  return (
    <Styles.IconLink
      variant="h5"
      color="secondary"
      underline="none"
      {...props}
      component={NextLink}
      href={href}
    >
      <PetsIcon color="inherit" fontSize="large" />

      {gt('ttl-website')}
    </Styles.IconLink>
  );
}
