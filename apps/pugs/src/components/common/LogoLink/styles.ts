import Link from '@mui/material/Link';
import { withStyles } from 'tss-react/mui';

export const IconLink = withStyles(
  Link,
  (theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      fontWeight: 'bolder',

      '& > svg': {
        marginRight: `${theme.spacing(1)} !important`,
      },
    },
  }),
  { name: 'IconLink' }
);
