declare const __WEBPACK_DEFINE__: {
  ENV: string;
  LANGUAGES: string[];
};

declare module '@alienfast/i18next-loader?relativePathAsNamespace=true!*' {
  const contents: import('i18next').Resource;

  export default contents;
}
