import { Suspense } from 'react';
import { SnackbarProvider } from 'notistack';
import LinearProgress from '@mui/material/LinearProgress';
import GlobalStyles from '@mui/material/GlobalStyles';
import NoSsr from '@mui/material/NoSsr';
import createCache from '@emotion/cache';
import { CacheProvider } from '@emotion/react';
import { ThemeProvider as MuiThemeProvider } from '@mui/material/styles';

import useLazy from '~pugs/hooks/useLazy';
import useAppContext from './hooks';
import * as Types from './types';

export { useAppContext };

const muiCache = createCache({
  key: 'mui',
  prepend: true,
});

export default function AppProvider({ children }: Types.AppProviderProps) {
  const { theme: code } = useAppContext();

  const LazyThemeProvider = useLazy(async () => {
    const { default: theme } = await import(`~pugs/themes/${code}`);

    return (props: any) => (
      <>
        <GlobalStyles
          styles={{
            body: {
              margin: 0,
              padding: 0,
              background: theme.palette.background.default,
              color: theme.palette.text.primary,
            },
          }}
        />

        <MuiThemeProvider {...props} theme={theme} />
      </>
    );
  }, [code]);

  return (
    <NoSsr>
      <Suspense fallback={<LinearProgress />}>
        <CacheProvider value={muiCache}>
          <LazyThemeProvider>
            <SnackbarProvider
              anchorOrigin={{ horizontal: 'center', vertical: 'top' }}
              maxSnack={3}
            >
              {children}
            </SnackbarProvider>
          </LazyThemeProvider>
        </CacheProvider>
      </Suspense>
    </NoSsr>
  );
}
