import i18n from 'i18next';
import zs from 'zustand';

import resources from '@alienfast/i18next-loader?relativePathAsNamespace=true!~pugs/assets/locales';
import * as Types from './types';

//* i18next
const initialLng: string =
  global.localStorage?.getItem('lng') || __WEBPACK_DEFINE__.LANGUAGES[0];

i18n.init({
  keySeparator: '.',
  resources,
  lng: initialLng,
  fallbackLng: {
    default: __WEBPACK_DEFINE__.LANGUAGES,
  },
  interpolation: {
    escapeValue: false,
    formatSeparator: ',',
  },
});

//* export
export default zs<Types.AppContextValue>((set) => ({
  theme: (global.localStorage?.getItem('theme.code') ||
    'default') as Types.AppContextValue['theme'],
  setTheme: (theme) => {
    global.document
      ?.querySelectorAll('style[data-emotion="css-global"]')
      .forEach((el) => el.remove());

    global.localStorage?.setItem('theme.code', theme);

    set({ theme });
  },

  actions: null,
  setActions: (actions) => set({ actions }),

  language: initialLng,
  getFixedT: (namespace) => i18n.getFixedT(null, namespace),
  setLanguage: (language) =>
    set(({ language: current }) => {
      const newLng = __WEBPACK_DEFINE__.LANGUAGES.includes(language)
        ? language
        : current;

      if (newLng !== current) {
        i18n.changeLanguage(newLng);
        global.localStorage?.setItem('lng', newLng);
      }

      return { language: newLng };
    }),
}));
