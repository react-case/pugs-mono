import { ReactNode } from 'react';
import { TFunction } from 'i18next';

type Theme = 'default';

export interface AppContextValue {
  theme: Theme;
  setTheme: (_theme: Theme) => void;

  actions: ReactNode;
  setActions: (_actions: ReactNode) => void;

  language: string;
  setLanguage: (_language: string) => void;
  getFixedT: (_namespace: string) => TFunction<string, string>;
}

export interface AppProviderProps {
  children: ReactNode;
}
