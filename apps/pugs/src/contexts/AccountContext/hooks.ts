import zs from 'zustand';

import { initial, login, signup } from '~pugs/services/account';
import * as Types from './types';

//* export
export default zs<Types.AccountContextValue>((set) => ({
  userinfo: null,
  isAuthenticated: false,

  initial: () =>
    initial().then((userinfo) => {
      set({ isAuthenticated: true, userinfo });

      return userinfo;
    }),
  onSignup: signup,

  onLogin: async (account) => {
    const userinfo = await login(account);

    set({ isAuthenticated: true, userinfo });

    return userinfo;
  },
}));
