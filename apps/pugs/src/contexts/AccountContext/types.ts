import { ReactNode } from 'react';

import { AccountServiceTypes } from '~pugs/services/account';

export type Account = AccountServiceTypes.Account;

export interface AccountContextValue {
  userinfo?: Omit<AccountServiceTypes.Account, 'password'>;
  isAuthenticated: boolean;
  initial: () => Promise<AccountContextValue['userinfo'] | null>;

  onLogin: (
    _account: Pick<AccountServiceTypes.Account, 'username' | 'password'>
  ) => Promise<AccountContextValue['userinfo']>;

  onSignup: AccountServiceTypes.Signup;
}

export interface AccountProviderProps {
  children: ReactNode;
}
