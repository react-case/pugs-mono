import AppBar from '@mui/material/AppBar';
import Container from '@mui/material/Container';
import { makeStyles, withStyles } from 'tss-react/mui';

export const Main = withStyles(
  Container,
  (theme) => ({
    root: {
      position: 'relative',
    },
  }),
  { name: 'Main' }
);

export const useAppMainStyles = makeStyles({ name: 'AppMain' })((theme) => ({
  appBar: {
    margin: theme.spacing(2, 0),
    borderRadius: theme.spacing(2),
    background: theme.palette.primary.dark,
  },
  logo: {
    marginRight: 'auto',
  },
  divider: {
    margin: theme.spacing(0, 2),
  },
}));

export const useSettingSpeedDialStyles = makeStyles({
  name: 'SettingSpeedDial',
})((theme) => ({
  dial: {
    position: 'absolute',
    top: theme.spacing(0.5),
    left: 0,
    transform: 'translateY(-25%)',

    '& > button': {
      background: 'none !important',
      boxShadow: 'none !important',
    },
  },
  dialIcon: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 'fit-content',
  },
  container: {
    position: 'relative',
    width: theme.spacing(6),
  },
}));
