import AppBar from '@mui/material/AppBar';
import Divider from '@mui/material/Divider';
import Toolbar from '@mui/material/Toolbar';

import LogoLink from '~pugs/components/common/LogoLink';
import SettingSpeedDial from './SettingSpeedDial';
import * as Styles from './styles';
import * as Types from './types';

export default function AppMain({ children }: Types.AppMainProps) {
  const { classes } = Styles.useAppMainStyles();

  return (
    <Styles.Main maxWidth="lg" className="app" component="main">
      <AppBar position="static" className={classes.appBar}>
        <Toolbar variant="regular">
          <LogoLink className={classes.logo} />

          <Divider
            flexItem
            orientation="vertical"
            className={classes.divider}
          />

          <SettingSpeedDial />
        </Toolbar>
      </AppBar>

      {children}
    </Styles.Main>
  );
}
