import { useMemo } from 'react';
import { useRouter } from 'next/router';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';

import { useAppContext } from '~pugs/contexts/AppContext';

export default function NavMenuTabs() {
  const { pathname } = useRouter();
  const { getFixedT } = useAppContext();
  const at = getFixedT('app');

  const actived = useMemo(
    () => pathname.split('/')[1] || 'pugging',
    [pathname]
  );

  return (
    <Tabs indicatorColor="secondary" textColor="secondary" value={actived}>
      <Tab value="pugging" label={at('ttl-pugging')} />
    </Tabs>
  );
}
