import NextLink from 'next/link';
import SpeedDial from '@mui/material/SpeedDial';
import SpeedDialAction from '@mui/material/SpeedDialAction';
import SpeedDialIcon from '@mui/material/SpeedDialIcon';
import ManageAccountsOutlinedIcon from '@mui/icons-material/ManageAccountsOutlined';
import WidgetsOutlinedIcon from '@mui/icons-material/WidgetsOutlined';

import { useAccountContext } from '~pugs/contexts/AccountContext';
import { useAppContext } from '~pugs/contexts/AppContext';
import Thumbnail from '~pugs/components/common/Thumbnail';
import * as Styles from './styles';

export default function SettingSpeedDial() {
  const { getFixedT } = useAppContext();
  const { userinfo } = useAccountContext();
  const { classes } = Styles.useSettingSpeedDialStyles();
  const at = getFixedT('app');

  return (
    <div className={classes.container}>
      <SpeedDial
        ariaLabel="SpeedDial openIcon example"
        direction="down"
        className={classes.dial}
        icon={
          <SpeedDialIcon
            className={classes.dialIcon}
            openIcon={<WidgetsOutlinedIcon />}
            icon={
              <Thumbnail
                background="none"
                size="small"
                src={userinfo?.thumbnail}
              />
            }
          />
        }
      >
        <SpeedDialAction
          icon={<ManageAccountsOutlinedIcon />}
          tooltipTitle={at('ttl-profile')}
          FabProps={
            {
              component: NextLink,
              href: {
                pathname: '/profile',
                query: { id: userinfo?.id, username: userinfo?.username },
              },
            } as any
          }
        />
      </SpeedDial>
    </div>
  );
}
