import { ReactNode } from 'react';

export interface WrapperProps {
  children: ReactNode;
}

export interface ServiceWorkerWrapperProps extends WrapperProps {
  onInit?: () => Promise<void>;
}
