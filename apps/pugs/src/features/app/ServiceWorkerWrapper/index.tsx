import { Suspense } from 'react';
import LinearProgress from '@mui/material/LinearProgress';

import { useAccountContext } from '~pugs/contexts/AccountContext';
import useLazy from '~pugs/hooks/useLazy';
import * as Types from './types';

export default function ServiceWorkerWrapper({
  children,
  onInit,
}: Types.ServiceWorkerWrapperProps) {
  const { isAuthenticated } = useAccountContext();

  const LazyWebSocket = useLazy(async () => {
    if (global.navigator && 'serviceWorker' in global.navigator) {
      await global.navigator.serviceWorker.register('/websocket.worker.js', {
        scope: '/',
      });

      await onInit?.();
    }

    return ({ children }: Types.WrapperProps) => <>{children}</>;
  }, [isAuthenticated]);

  return (
    <Suspense fallback={<LinearProgress />}>
      <LazyWebSocket>{children}</LazyWebSocket>
    </Suspense>
  );
}
