import IconButton from '@mui/material/IconButton';
import Slide from '@mui/material/Slide';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';

import * as Styles from './styles';
import * as Types from './types';

export default function NavMenuDrawer({
  open,
  subheader,
  onClose,
}: Types.NavMenuDrawerProps) {
  return (
    <Styles.NavDrawer
      variant="permanent"
      open={open}
      ModalProps={{ keepMounted: true }}
    >
      <Slide direction="down" in={open}>
        <Styles.NavSubheader variant="regular">
          {subheader}

          <IconButton role="close" color="secondary" onClick={onClose}>
            <ChevronLeftIcon />
          </IconButton>
        </Styles.NavSubheader>
      </Slide>

      <Slide direction="up" in={open}>
        <Styles.NavList
          sx={(theme) => ({
            borderBottomRightRadius: theme.spacing(2),
          })}
        ></Styles.NavList>
      </Slide>
    </Styles.NavDrawer>
  );
}
