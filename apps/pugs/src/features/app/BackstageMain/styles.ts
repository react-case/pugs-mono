import AppBar from '@mui/material/AppBar';
import Container from '@mui/material/Container';
import Drawer from '@mui/material/Drawer';
import Link from '@mui/material/Link';
import List from '@mui/material/List';
import Toolbar from '@mui/material/Toolbar';
import { withStyles } from 'tss-react/mui';

export const DividerAppBar = withStyles(
  AppBar,
  (theme) => ({
    root: {
      borderBottom: `1px solid ${theme.palette.divider}`,
      boxShadow: 'none !important',
    },
  }),
  { name: 'DividerAppBar' }
);

export const HomeLink = withStyles(
  Link,
  (theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      fontWeight: 'bolder',

      '& > svg': {
        marginRight: `${theme.spacing(2)} !important`,
      },
    },
  }),
  { name: 'HomeLink' }
);

export const Main = withStyles(
  Container,
  (theme) => ({
    root: {
      position: 'relative',
      display: 'flex',
      flexDirection: 'column',
      width: '100vw',
      height: '100vh',
      overflow: 'hidden !important',

      '& *::-webkit-scrollbar': {
        width: theme.spacing(0.5),
        height: theme.spacing(0.5),
      },
      '& *::-webkit-scrollbar-track': {
        background: theme.palette.background.paper,
        borderRadius: theme.shape.borderRadius,
      },
      '& *::-webkit-scrollbar-thumb': {
        background: theme.palette.primary.main,
        borderRadius: theme.shape.borderRadius,
      },
      '& *::-webkit-scrollbar-thumb:hover': {
        background: theme.palette.secondary.main,
      },
    },
  }),
  { name: 'Main' }
);

export const Content = withStyles(
  Container,
  (theme) => ({
    root: {
      paddingBottom: theme.spacing(2),
      paddingTop: theme.spacing(2),
    },
  }),
  { name: 'Content' }
);

export const NavDrawer = withStyles(
  Drawer,
  (theme, { open }) => ({
    root: {
      position: 'fixed',
      overflow: 'hidden',
      zIndex: theme.zIndex.drawer,
      top: 0,
      left: 0,
      width: theme.spacing(open ? 40 : 0),
      height: '100vh',

      [theme.breakpoints.only('xs')]: {
        width: open ? '90vw' : 0,
      },
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration:
          theme.transitions.duration[open ? 'enteringScreen' : 'leavingScreen'],
      }),
    },
    paper: {
      position: 'static !important' as 'static',
      display: 'flex',
      flexDirection: 'column' as any,
      width: '100%',
      height: '100%',
      overflow: 'hidden',
      background: 'none',

      '& > *': {
        background: theme.palette.background.paper,
        borderRight: `1px solid ${theme.palette.divider}`,
      },
    },
  }),
  { name: 'NavDrawer' }
);

export const NavSubheader = withStyles(
  Toolbar,
  (theme) => ({
    root: {
      borderTopRightRadius: theme.spacing(2),
      borderBottom: `1px solid ${theme.palette.divider}`,

      '& > button[role=close]': {
        marginLeft: 'auto',
      },
    },
  }),
  { name: 'NavSubheader' }
);

export const NavList = withStyles(
  List,
  (theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
  }),
  { name: 'NavList' }
);
