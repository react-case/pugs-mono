import { MouseEventHandler, ReactNode } from 'react';

export interface BackstageMainProps {
  children: ReactNode;
}

export interface NavMenuDrawerProps {
  open: boolean;
  subheader?: ReactNode;
  onClose: MouseEventHandler<HTMLButtonElement>;
}
