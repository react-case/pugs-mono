import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import ClickAwayListener from '@mui/base/ClickAwayListener';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import WidgetsIcon from '@mui/icons-material/Widgets';

import LogoLink from '~pugs/components/common/LogoLink';
import ManagementNavMenuDrawer from '~pugs/features/app/BackstageMain/NavMenuDrawer';
import { useAppContext } from '~pugs/contexts/AppContext';
import * as Styles from './styles';
import * as Types from './types';

export default function BackstageMain({ children }: Types.BackstageMainProps) {
  const { asPath } = useRouter();
  const { getFixedT } = useAppContext();
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const gt = getFixedT('app');

  useEffect(() => {
    setIsMenuOpen(false);
  }, [asPath]);

  return (
    <Styles.Main
      disableGutters
      maxWidth={false}
      className="app"
      component="main"
    >
      <ClickAwayListener onClickAway={() => setIsMenuOpen(false)}>
        <Styles.DividerAppBar position="sticky" color="default">
          <Toolbar variant="regular">
            {!isMenuOpen && (
              <>
                <IconButton
                  color="primary"
                  onClick={() => setIsMenuOpen(!isMenuOpen)}
                  style={{ marginRight: 8 }}
                >
                  <WidgetsIcon />
                </IconButton>

                <Styles.HomeLink
                  variant="h5"
                  color="primary"
                  underline="none"
                  component={NextLink}
                  href="/"
                >
                  {gt('ttl-website')}
                </Styles.HomeLink>
              </>
            )}
          </Toolbar>

          <ManagementNavMenuDrawer
            open={isMenuOpen}
            onClose={() => setIsMenuOpen(false)}
            subheader={<LogoLink href="/backstage" />}
          />
        </Styles.DividerAppBar>
      </ClickAwayListener>

      <Styles.Content maxWidth="lg">{children}</Styles.Content>
    </Styles.Main>
  );
}
