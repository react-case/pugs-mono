import { MouseEventHandler } from 'react';

export type SwipeStatus = 'login' | 'signup';

export interface AuthSwipeProps {
  onLoginSuccess?: () => void;
}

export interface AuthAlertProps {
  role?: string;
  message: string;
  buttonText: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
}

export interface LoginFormData {
  username: string;
  password: string;
}

export interface SignupFormdData extends LoginFormData {
  fullname: string;
  image?: string;
}

export interface AuthFormProps<Formdata extends LoginFormData> {
  onSubmit: (_e: Formdata) => void;
}
