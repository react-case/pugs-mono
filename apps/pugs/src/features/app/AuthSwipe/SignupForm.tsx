import { useState } from 'react';
import Button from '@mui/material/Button';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Divider from '@mui/material/Divider';
import Paper from '@mui/material/Paper';
import PersonAddAltOutlinedIcon from '@mui/icons-material/PersonAddAltOutlined';
import TextField from '@mui/material/TextField';

import { useAppContext } from '~pugs/contexts/AppContext';
import { useFormValid } from './hooks';
import * as Styles from './styles';
import * as Types from './types';

export default function SignupForm({
  onSubmit,
}: Types.AuthFormProps<Types.SignupFormdData>) {
  const { getFixedT } = useAppContext();
  const [passwords, setPasswords] = useState({ value: '', confirm: '' });
  const at = getFixedT('app');
  const isPasswordInvalid = passwords.value !== passwords.confirm;

  const [formRef, getData, isFormValid] = useFormValid<Types.SignupFormdData>(
    () => !isPasswordInvalid
  );

  const handleSubmit = (e) => {
    e.preventDefault();

    if (isFormValid()) {
      onSubmit(getData());
    }
  };

  return (
    <Styles.FormCard
      ref={formRef as any}
      elevation={0}
      component="form"
      onSubmit={handleSubmit}
    >
      <CardHeader
        role="title"
        component={Paper}
        avatar={
          <PersonAddAltOutlinedIcon
            color="secondary"
            style={{ fontSize: 48 }}
          />
        }
        titleTypographyProps={{ variant: 'h5', color: 'text.primary' }}
        title={at('ttl-signup-form')}
        subheader={at('ttl-signup-form-sub')}
      />

      <CardContent role="content" component={Paper}>
        <TextField
          fullWidth
          variant="outlined"
          margin="normal"
          color="secondary"
          name="username"
          label={at('lbl-username')}
          placeholder={at('plh-username')}
          InputProps={{ inputProps: { required: true } }}
        />

        <TextField
          fullWidth
          variant="outlined"
          margin="normal"
          color="secondary"
          name="fullname"
          label={at('lbl-fullname')}
          placeholder={at('plh-fullname')}
          InputProps={{ inputProps: { required: true } }}
        />

        <Divider />

        <TextField
          fullWidth
          variant="outlined"
          color="secondary"
          type="password"
          name="password"
          label={at('lbl-password')}
          onChange={(e) => setPasswords({ value: e.target.value, confirm: '' })}
          InputProps={{ inputProps: { required: true } }}
        />

        <TextField
          key={passwords.value}
          fullWidth
          variant="outlined"
          color="secondary"
          type="password"
          label={at('lbl-confirm-password')}
          error={isPasswordInvalid}
          helperText={isPasswordInvalid && at('msg-invalid-password')}
          onChange={(e) =>
            setPasswords(({ value }) => ({ value, confirm: e.target.value }))
          }
          InputProps={{
            inputProps: { required: true, 'aria-invalid': isPasswordInvalid },
          }}
        />
      </CardContent>

      <CardActions role="actions" disableSpacing>
        <Button
          fullWidth
          type="submit"
          variant="contained"
          size="large"
          color="secondary"
        >
          {at('btn-signup')}
        </Button>
      </CardActions>
    </Styles.FormCard>
  );
}
