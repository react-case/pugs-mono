import { useSnackbar } from 'notistack';
import Slide from '@mui/material/Slide';

import { useAppContext } from '~pugs/contexts/AppContext';
import { useAccountContext } from '~pugs/contexts/AccountContext';
import { useStatus } from './hooks';
import * as Styles from './styles';
import * as Types from './types';
import AuthAlert from './AuthAlert';
import LoginForm from './LoginForm';
import SignupForm from './SignupForm';

export default function AuthSwipe({ onLoginSuccess }: Types.AuthSwipeProps) {
  const { enqueueSnackbar } = useSnackbar();
  const { getFixedT } = useAppContext();
  const { onLogin, onSignup } = useAccountContext();
  const [status, setStatue] = useStatus();
  const at = getFixedT('app');

  return (
    <Styles.MainBackdrop open>
      <Styles.RelativeContainer maxWidth="xs">
        <Slide direction="down" in={status === 'signup'}>
          <AuthAlert
            role="auth-login"
            message={at('msg-login-tip')}
            buttonText={at('btn-login')}
            onClick={() => setStatue('login')}
          />
        </Slide>

        <Slide direction="up" in={status === 'login'}>
          <AuthAlert
            role="auth-signup"
            message={at('msg-signup-tip')}
            buttonText={at('btn-signup')}
            onClick={() => setStatue('signup')}
          />
        </Slide>

        <Styles.AbsoluteContent
          sx={(theme) => ({ top: theme.spacing(status === 'login' ? 3 : 17) })}
        >
          <Styles.FullCollapse in={status === 'login'}>
            <LoginForm
              key={status}
              onSubmit={(data) =>
                onLogin(data)
                  .then(() => onLoginSuccess?.())
                  .catch((err) =>
                    enqueueSnackbar(at(err), { variant: 'error' })
                  )
              }
            />
          </Styles.FullCollapse>

          <Styles.FullCollapse in={status === 'signup'}>
            <SignupForm
              key={status}
              onSubmit={(data) => onSignup(data).then(() => setStatue('login'))}
            />
          </Styles.FullCollapse>
        </Styles.AbsoluteContent>
      </Styles.RelativeContainer>
    </Styles.MainBackdrop>
  );
}
