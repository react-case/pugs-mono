import { forwardRef } from 'react';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

import * as Types from './types';

const AuthAlert = forwardRef<HTMLDivElement, Types.AuthAlertProps>(
  ({ role, message, buttonText, onClick }, ref) => (
    <Container {...{ ref, role }}>
      <Typography paragraph variant="h5" color="secondary" align="center">
        {message}
      </Typography>

      <Button
        variant="outlined"
        color="secondary"
        size="large"
        onClick={onClick}
      >
        {buttonText}
      </Button>
    </Container>
  )
);

AuthAlert.displayName = 'AuthAlert';

export default AuthAlert;
