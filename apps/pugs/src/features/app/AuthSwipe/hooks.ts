import { Dispatch, Ref, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';

import * as Types from './types';

export const useStatus = (): [
  Types.SwipeStatus,
  Dispatch<Types.SwipeStatus>
] => {
  const { pathname, query, replace } = useRouter();

  const [status, setStatue] = useState<Types.SwipeStatus>(
    (query.status as Types.SwipeStatus) || 'login'
  );

  useEffect(() => {
    if (status !== query.status) {
      replace({ pathname, query: { status } });
    }
  }, [status, query.status]);

  return [status, setStatue];
};

export const useFormValid = <Formdata>(
  verify?: (data: Formdata) => boolean
): [Ref<HTMLFormElement>, () => Formdata, () => boolean] => {
  const formRef = useRef<HTMLFormElement>();

  const getData = (): Formdata => {
    const formdata = new FormData(formRef.current);
    const data: Record<string, any> = {};

    formdata.forEach((value, key) => (data[key] = value));

    return data as Formdata;
  };

  return [
    formRef,
    getData,

    () =>
      formRef.current?.checkValidity() &&
      (verify instanceof Function ? verify(getData()) === true : true),
  ];
};
