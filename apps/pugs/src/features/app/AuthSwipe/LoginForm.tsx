import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import Button from '@mui/material/Button';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';

import { useAppContext } from '~pugs/contexts/AppContext';
import { useFormValid } from './hooks';
import * as Styles from './styles';
import * as Types from './types';

export default function LoginForm({
  onSubmit,
}: Types.AuthFormProps<Types.LoginFormData>) {
  const { getFixedT } = useAppContext();
  const at = getFixedT('app');

  const [formRef, getData, isFormValid] = useFormValid<Types.LoginFormData>();

  const handleSubmit = (e) => {
    e.preventDefault();

    if (isFormValid()) {
      onSubmit(getData());
    }
  };

  return (
    <Styles.FormCard
      ref={formRef as any}
      elevation={0}
      component="form"
      onSubmit={handleSubmit}
    >
      <CardHeader
        role="title"
        component={Paper}
        avatar={
          <AccountCircleOutlinedIcon
            color="secondary"
            style={{ fontSize: 48 }}
          />
        }
        titleTypographyProps={{ variant: 'h5', color: 'text.primary' }}
        title={at('ttl-login-form')}
        subheader={at('ttl-login-form-sub')}
      />

      <CardContent role="content" component={Paper}>
        <TextField
          fullWidth
          variant="outlined"
          color="secondary"
          name="username"
          label={at('lbl-username')}
          placeholder={at('plh-username')}
          InputProps={{ inputProps: { required: true } }}
        />

        <TextField
          fullWidth
          variant="outlined"
          color="secondary"
          type="password"
          name="password"
          label={at('lbl-password')}
          InputProps={{ inputProps: { required: true } }}
        />
      </CardContent>

      <CardActions role="actions" disableSpacing>
        <Button
          fullWidth
          type="submit"
          variant="contained"
          size="large"
          color="secondary"
        >
          {at('btn-login')}
        </Button>
      </CardActions>
    </Styles.FormCard>
  );
}
