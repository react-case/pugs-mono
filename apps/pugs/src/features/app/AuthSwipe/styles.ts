import Backdrop from '@mui/material/Backdrop';
import Card from '@mui/material/Card';
import Collapse from '@mui/material/Collapse';
import Container from '@mui/material/Container';
import { withStyles } from 'tss-react/mui';

export const MainBackdrop = withStyles(
  Backdrop,
  (theme) => ({
    root: {
      backdropFilter: `blur(${theme.spacing(0.5)})`,
    },
  }),
  { name: 'MainBackdrop' }
);

export const RelativeContainer = withStyles(
  Container,
  (theme) => ({
    root: {
      position: 'relative',
      display: 'flex',
      flexDirection: 'column',
      flexWrap: 'nowrap',
      background: theme.palette.background.paper,
      borderRadius: theme.spacing(3),
      height: '90%',
      margin: theme.spacing(6),

      '& > *[role^="auth-"]': {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        width: '100%',
        padding: theme.spacing(3, 0),
        alignItems: 'center',

        '&[role=auth-login]': {
          justifyContent: 'flex-start',
        },
        '&[role=auth-signup]': {
          justifyContent: 'flex-end',
        },
      },
    },
  }),
  { name: 'RelativeContainer' }
);

export const AbsoluteContent = withStyles(
  Container,
  (theme) => ({
    root: {
      position: 'absolute',
      background: theme.palette.primary.dark,
      borderRadius: theme.spacing(1.5),
      maxWidth: `calc(100vw - ${theme.spacing(2)})`,
      width: `calc(100% + ${theme.spacing(16)})`,
      height: `calc(100% - ${theme.spacing(20)})`,
      padding: theme.spacing(2, 3),
      left: '50%',
      transform: 'translateX(-50%)',

      transition: theme.transitions.create('top', {
        easing: theme.transitions.easing.sharp,
        duration:
          theme.transitions.duration[open ? 'enteringScreen' : 'leavingScreen'],
      }),
    },
  }),
  { name: 'AbsoluteContent' }
);

export const FullCollapse = withStyles(
  Collapse,
  (theme, { in: open }) => ({
    root: {
      opacity: open ? 1 : 0,
      height: open ? '100% !important' : 0,

      transition: theme.transitions.create(['opacity', 'height'], {
        easing: theme.transitions.easing.sharp,
        duration:
          theme.transitions.duration[open ? 'enteringScreen' : 'leavingScreen'],
      }),
    },
    wrapper: {
      height: '100%',
    },
    wrapperInner: {
      height: '100%',
    },
  }),
  { name: 'FullCollapse' }
);

export const FormCard = withStyles(
  Card,
  (theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'column',
      background: 'none !important',
      width: '100%',
      height: '100%',
      overflow: 'hidden auto',

      '& > * + *': {
        marginTop: theme.spacing(1.5),
      },
      '& > *[role=title]': {
        position: 'sticky',
        top: 0,
        zIndex: 10,
      },
      '& > *[role=content]': {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        padding: theme.spacing(3, 2),
        height: '100%',

        '& > *': {
          margin: theme.spacing(2, 0, 1, 0),
        },
      },
      '& > *[role=actions]': {
        padding: 0,
      },
    },
  }),
  { name: 'FormCard' }
);
