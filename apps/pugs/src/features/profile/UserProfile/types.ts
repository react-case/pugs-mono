import { AccountServiceTypes } from '~pugs/services/account';

export interface ProfileCardProps {
  userinfo?: Omit<AccountServiceTypes.Account, 'password' | 'token'>;
}

export interface ProfileCardHeader {
  userinfo?: Omit<AccountServiceTypes.Account, 'password' | 'token'>;
}
