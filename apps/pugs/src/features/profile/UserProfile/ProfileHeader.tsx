import CardHeader from '@mui/material/CardHeader';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';

import { useAppContext } from '~pugs/contexts/AppContext';
import Thumbnail from '~pugs/components/common/Thumbnail';
import * as Styles from './styles';
import * as Types from './types';

export default function ProfileCardHeader({
  userinfo,
}: Types.ProfileCardHeader) {
  const { getFixedT } = useAppContext();
  const { classes } = Styles.useProfileCardHeaderStyles();
  const pt = getFixedT('profile');

  return (
    <CardHeader
      classes={{ avatar: classes.avatar }}
      titleTypographyProps={{ variant: 'h5', color: 'text.primary' }}
      title={userinfo?.fullname || pt('msg-invalid-user')}
      subheader={userinfo?.description || pt('msg-no-user-description')}
      avatar={
        <Thumbnail
          background="primary"
          size="large"
          className={classes.thumbnail}
          src={userinfo?.thumbnail}
          {...(!userinfo && { icon: QuestionMarkIcon })}
        />
      }
    />
  );
}
