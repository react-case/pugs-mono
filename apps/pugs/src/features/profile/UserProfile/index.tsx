import Card from '@mui/material/Card';
import Paper from '@mui/material/Paper';

import MiddleCardMedia from '~pugs/components/common/MiddleCardMedia';
import ResponsiveImage from '~pugs/components/common/ResponsiveImage';
import IMG_DEFAULT_BACKGROUND from '~pugs/assets/imgs/default-background.png';
import ProfileHeader from './ProfileHeader';
import * as Styles from './styles';
import * as Types from './types';

export default function ProfileCard({ userinfo }: Types.ProfileCardProps) {
  return (
    <Styles.ProfileContainer disableGutters maxWidth="lg" component={Card}>
      <MiddleCardMedia sx={{ maxHeight: '30vh' }}>
        {!userinfo ? (
          <Paper
            square
            sx={(theme) => ({
              background: theme.palette.action.disabledBackground,
              width: '100%',
              height: '30vh',
            })}
          />
        ) : (
          <ResponsiveImage
            alt="profile-media"
            src={userinfo.background || IMG_DEFAULT_BACKGROUND}
          />
        )}
      </MiddleCardMedia>

      <ProfileHeader userinfo={userinfo} />
    </Styles.ProfileContainer>
  );
}
