import Container from '@mui/material/Container';

import { makeStyles, withStyles } from 'tss-react/mui';

export const ProfileContainer = withStyles(
  Container,
  (theme) => ({ root: { borderRadius: theme.spacing(2) } }),
  { name: 'ProfileContainer' }
);

export const useProfileCardHeaderStyles = makeStyles({
  name: 'ProfileCardHeader',
})((theme) => ({
  avatar: {
    position: 'relative',
    width: theme.spacing(15),
    height: theme.spacing(5),
  },
  thumbnail: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    zIndex: 1,
    boxShadow: theme.shadows[5],
  },
}));
